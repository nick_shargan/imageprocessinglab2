#include <string>
#include <iostream>
#include <GL\glut.h>
#include <gdcmReader.h>
#include <gdcmImageReader.h>
#include <gdcmImage.h>
#include <gdcmFile.h>
#include <gdcmAttribute.h>

using namespace std;
using namespace gdcm;
#pragma comment(lib,"gdcmMSFF.lib")
#pragma comment(lib,"gdcmDSED.lib")

GLushort *rgb_buffer;
const unsigned int *dims;
GLuint tex;
string value;


bool LoadFromFile(const char* fileName, GLushort* imageData)
{
    FILE* fi(fopen(fileName, "rb"));
    if(fi)
    {
        int size = dims[0] * dims[1];
        fseek(fi, -(size * 2), SEEK_END);
        fread(imageData, sizeof(GLushort), size, fi);
        fclose(fi);
        int max = 0;
        for(int pixNo = 0; pixNo < size; pixNo++)
            if(imageData[pixNo] > max)
                max = imageData[pixNo];
        for(int pixNo = 0; pixNo < size; pixNo++)
            imageData[pixNo] *= 65535 / max;
	return true;
}
  else
	  return false;
}

void display()
{
	glDrawPixels(dims[0], dims[1], GL_RGB, GL_UNSIGNED_SHORT, rgb_buffer); 
    glFlush();
}

void keyPressed (unsigned char key, int x, int y) 
{
	if(key == '1')
	    cout << "Tag=" << value << endl;
}

int main(int argc, char* argv[])
{
	char filename[] = "img/IM-0001-0001.dcm";

	gdcm::ImageReader reader;
	reader.SetFileName(filename);
	if(!reader.Read())
    {
	    std::cerr << "Could not read: " << filename << std::endl;
	    return 1;
    }
	File &file = reader.GetFile();
	const Image &image = reader.GetImage();
	unsigned int ndim = image.GetNumberOfDimensions();
	dims = image.GetDimensions();
	
	DataSet &ds = file.GetDataSet();
	Attribute <0x0008,0x1090> at;
    at.SetFromDataSet(ds);
    value = at.GetValue();
	//double v = at.GetValue();

	GLushort* imageData = new GLushort[dims[0] * dims[1]];
	LoadFromFile(filename, imageData);
	
	rgb_buffer = new GLushort[dims[0] * dims[1] * 3];
	for(int i = 0; i < dims[0] * dims[1]; i++)
		rgb_buffer[i * 3] = rgb_buffer[i * 3 + 1] = rgb_buffer[i * 3 + 2] = imageData[i];

	glOrtho(0, 0, dims[0], dims[1], -1, 1);
	glutInit(&argc, argv); 
	glutInitDisplayMode (GLUT_RGB); 
	glutInitWindowSize (512, 512); 
	glutCreateWindow ("Lab 2"); 
	glutDisplayFunc(display); 
	glutKeyboardFunc(keyPressed);
	glutMainLoop(); 
	return 1;
}